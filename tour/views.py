from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.shortcuts import render, get_object_or_404, redirect
from django.views.generic import TemplateView, DetailView, ListView
from django.views.generic.edit import CreateView, DeleteView
from tour.models import *
from .forms import *
from .models import Comment, Contact
from django.contrib.auth import get_user_model
from users.models import CustomUser
from taggit.models import Tag
from datetime import datetime
from django.db.models import Q


class BookingView(TemplateView):
    template_name = "pages/book-trip.html"

    def get(self, request, *args, **kwargs):
        tours = Tour.objects.all()

        name = request.user
        booking_form = BookingForm()
        return render(request, self.template_name, context={
            'tours': tours,
            'name': name,
            'booking_form': booking_form,
        })

    def post(self, request, *args, **kwargs):
        tour = request.POST.get("id", "")
        tour = Tour.objects.get(pk=tour)
        booking = Booking
        new_booking = None
        booking_form = BookingForm(data=request.POST)
        if booking_form.is_valid():
            print('postko keldi')
            new_booking = booking_form.save(commit=False)
            new_booking.tour = tour
            new_booking.save()
        else:
            print('no valid!!!!!!!!!!!')
            booking_form = BookingForm()
        return redirect('booking')

class TourList(TemplateView):
    template_name = "tour/tour_list.html"

    def get(self, request, tag_slug=None, category_id=None, *args):
        all_tours = Tour.objects.all()
        last_tours = []
        if Tour.objects.all():
            last_tours = Tour.objects.order_by('-created')[:3]
        object_list = Tour.objects.all()
        popular_tours = Tour.objects.filter(popular=True)
        all_tags = Tour.tags.all()
        all_category = Category.objects.all()
        current_time = datetime.now()
        tag = None
        category = None

        if tag_slug:
            tag = get_object_or_404(Tag, slug=tag_slug)
            object_list = object_list.filter(tags__in=[tag])
        if category_id:
            category = get_object_or_404(Category, id=category_id)
            object_list = object_list.filter(category__in=[category])
        paginator = Paginator(object_list, 2)  # По 1 статьи на каждой странице.
        page = request.GET.get('page')
        try:
            tours = paginator.page(page)
        except PageNotAnInteger:
            # Если указанная страница не является целым числом.
            tours = paginator.page(1)
        except EmptyPage:
            # Если указанный номер больше, чем всего страниц, возвращаем последнюю.
            tours = paginator.page(paginator.num_pages)
        return render(request, self.template_name, context={
            'tours': tours,
            'tag': tag,
            'page': page,
            'current_time': current_time,
            'popular_tours': popular_tours,
            'all_tours': all_tours,
            'all_tags': all_tags,
            'all_category': all_category,
            'category': category,
            'last_tours': last_tours,
        })


def detail_of_tour(request, id):
    tour = get_object_or_404(Tour, id=id)
    popular_tours = Tour.objects.filter(popular=True)
    all_tags = Tour.tags.all()
    all_category = Category.objects.all()

    comments = tour.comments.filter(active=True)
    comment_count = comments.count()

    new_comment = None
    if request.method == 'POST':
        comment_form = CommentForm(data=request.POST)

        if comment_form.is_valid():
            new_comment = comment_form.save(commit=False)
            new_comment.name = request.user
            new_comment.tour = tour
            new_comment.save()
            return redirect('tour_detail', id=id)
        else:
            comment_form.username = 'Signup'
    else:
        tour.views += 1
        tour.save()
        comment_form = CommentForm()

    return render(request, 'tour/tour_detail.html', {'tour': tour,
                                                     'comments': comments,
                                                     'new_comment': new_comment,
                                                     'comment_form': comment_form,
                                                     'popular_tours': popular_tours,
                                                     'all_category': all_category,
                                                     'all_tags': all_tags,
                                                     'comment_count': comment_count,
                                                     })

class ContactListView(ListView):
    model = Contact
    template_name = 'tour/contact.html'


def about_detail(request):
    about = []
    if About.objects.all():
        about = About.objects.all()[0]
    members = Members.objects.all()
    comments = Comment.objects.filter(active=True)
    popular_tours = Tour.objects.filter(popular=True)
    comments_texts = []
    for c in comments:
        comments_texts.append(c.text)

    return render(request, 'pages/about-us.html', {'about': about,
                                                   'members': members,
                                                   'comments_texts': comments_texts,
                                                   'popular_tours': popular_tours,
                                                   })


class MembersListView(ListView):
    model = Members
    template_name = 'pages/about-us.html'
    context_object_name = 'members'


class SearchView(TemplateView):
    template_name = "tour/result.html"

    def get(self, request, *args, **kwargs):

        search = request.GET.get('search')
        tours = Tour.objects.all()
        # results = tours.filter(Q(title__icontains=search) | Q(text__icontains=search))
        results = []
        for i in tours:
            if search.lower() in i.title.lower() or search.lower() in i.text.lower():
                results.append(i)
        return render(request, self.template_name, context={
            'results':results
        })

    def post(self, request, *args, **kwargs):
        return render(request, self.template_name, context={
        })

from django.contrib import admin
from .models import *


class TourImageInline(admin.TabularInline):
    model = Images
    extra = 2

@admin.register(Tour)
class TourAdmin(admin.ModelAdmin):
    list_filter = ('title', 'category', 'popular', 'famous')
    list_display = ('title', 'category', 'popular', 'famous')
    inlines = [TourImageInline]

@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ('category',)

@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
    model = Comment
    list_display = ('name', 'text', 'active')




@admin.register(Members)
class MembersAdmin(admin.ModelAdmin):
    list_display = ('name', 'position')
    list_filter = ('name', 'position')

@admin.register(Booking)
class BookingAdmin(admin.ModelAdmin):
    list_display = ('tour', 'name',)
    list_filter = ('tour', 'name',)

@admin.register(Contact)
class ContactAdmin(admin.ModelAdmin):
    list_display = ('address', 'address_text', 'number', 'number_text', 'email', 'email_text')

class AboutImageInline(admin.TabularInline):
    model = Cover
    extra = 4

@admin.register(About)
class AboutAdmin(admin.ModelAdmin):
    list_display = ('title',)
    list_filter = ('title',)
    inlines = [AboutImageInline]

@admin.register(Quotes)
class QuotesAdmin(admin.ModelAdmin):
    list_display = ('text',)
from .models import *
from django import forms


class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ('text',)

class BookingForm(forms.ModelForm):
    name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder':"Name"}))
    email = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder':"Email"}))
    number = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder':"Number"}))
    class Meta:
        model = Booking
        fields = ('name', 'email', 'number')
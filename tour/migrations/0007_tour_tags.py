# Generated by Django 3.0.2 on 2020-01-23 06:18

from django.db import migrations
import taggit.managers


class Migration(migrations.Migration):

    dependencies = [
        ('taggit', '0003_taggeditem_add_unique_index'),
        ('tour', '0006_auto_20200123_0217'),
    ]

    operations = [
        migrations.AddField(
            model_name='tour',
            name='tags',
            field=taggit.managers.TaggableManager(help_text='A comma-separated list of tags.', through='taggit.TaggedItem', to='taggit.Tag', verbose_name='Tags'),
        ),
    ]

# Generated by Django 3.0.2 on 2020-01-24 03:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tour', '0012_auto_20200124_0935'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contact',
            name='address',
            field=models.CharField(help_text='California, United States', max_length=200),
        ),
        migrations.AlterField(
            model_name='contact',
            name='address_text',
            field=models.CharField(help_text='Santa monica bullevard', max_length=200),
        ),
        migrations.AlterField(
            model_name='contact',
            name='email',
            field=models.EmailField(help_text='support@colorlib.com', max_length=254),
        ),
        migrations.AlterField(
            model_name='contact',
            name='email_text',
            field=models.CharField(help_text='Send us your query anytime!', max_length=200),
        ),
        migrations.AlterField(
            model_name='contact',
            name='number_text',
            field=models.CharField(help_text='Mon to Fri 9am to 6 pm', max_length=200),
        ),
    ]

from django.conf import settings
from django.conf.urls.static import static
from django.urls import path
from tour.views import *

urlpatterns = [
    path('tour/list', TourList.as_view(), name='tourlist'),
    path('tour/<int:id>/', detail_of_tour, name='tour_detail'),
    path('tag/<slug:tag_slug>/',TourList.as_view(), name='tour_list_by_tag'),
    path('category/<int:category_id>/',TourList.as_view(), name='tour_list_by_category'),
    path('contact/', ContactListView.as_view(), name='contact'),
    path('about/', about_detail, name='about'),
    path('members/', MembersListView.as_view(), name='members'),
    # path('booking/<int:id>/', BookingView.as_view(), name='new_booking'),
    path('booking/', BookingView.as_view(), name='booking'),
    path('search/', SearchView.as_view(), name='search'),
    # path('about/', AboutListView.as_view(), name='about'),
    # path('', AboutListView.as_view(), name='about'),
    # path('header/<int:pk>/', ContactDetailViewHeader.as_view(), name='header'),
]
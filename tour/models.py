from datetime import datetime

from django.contrib.auth import get_user_model
from django.db import models
from django.urls import reverse

from taggit.managers import TaggableManager

class Tour(models.Model):
    title = models.CharField(max_length=100)
    text = models.TextField()
    created = models.DateTimeField(auto_now_add=True)
    views = models.IntegerField(default=0)
    photo = models.ImageField(upload_to='images')
    description = models.TextField()
    category = models.ForeignKey('Category', on_delete=models.CASCADE, related_name='tour')
    tags = TaggableManager()
    popular = models.BooleanField(default=False)
    famous = models.BooleanField(default=False)
    start_day = models.IntegerField()
    start_month = models.CharField(max_length=15)
    how_many_days = models.CharField(max_length=50)


    def __str__(self):
        return f'{self.title}'

    def get_absolute_url(self):
        return reverse('tour_detail', args=[str(self.id)])

class Images(models.Model):
    tour = models.ForeignKey(Tour, on_delete=models.CASCADE, related_name='images')
    images = models.ImageField(upload_to='images')

class Category(models.Model):
    category = models.CharField(max_length=100)
    photo = models.ImageField(upload_to='images/categories')
    description = models.TextField()

    def __str__(self):
        return self.category

class Comment(models.Model):
    tour = models.ForeignKey(Tour, on_delete=models.CASCADE, related_name='comments')
    name = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    text = models.CharField(max_length=255)
    date = models.DateField(default=datetime.now)
    username = models.CharField(max_length=50)
    active = models.BooleanField(default=True)

    def __str__(self):
        return self.text

class About(models.Model):
    title = models.CharField(max_length=100)
    text = models.TextField()
    photo = models.ImageField(upload_to='images')


class Cover(models.Model):
    about = models.ForeignKey(About, on_delete=models.CASCADE, related_name='cover')
    images = models.ImageField(upload_to='images')
    title_cover = models.CharField(max_length=100)
    text_cover = models.CharField(max_length=100)

class Members(models.Model):
    photo = models.ImageField(upload_to='images/members')
    name = models.CharField(max_length=50)
    position = models.CharField(max_length=100)

class Booking(models.Model):
    tour = models.ForeignKey(Tour, on_delete=models.CASCADE, related_name='booking')
    name = models.CharField(max_length=50)
    email = models.EmailField()
    number = models.CharField(max_length=15, help_text='0700 12 34 56')

class Contact(models.Model):
    address = models.CharField(max_length=200, help_text='California, United States')
    address_text = models.CharField(max_length=200, help_text='Santa monica bullevard')
    number = models.CharField(max_length=15, help_text='0777 00 00 00')
    number_text = models.CharField(max_length=200, help_text='Mon to Fri 9am to 6 pm')
    email = models.EmailField(help_text='support@colorlib.com')
    email_text = models.CharField(max_length=200, help_text='Send us your query anytime!')

class Quotes(models.Model):
    text = models.TextField()

    def __str__(self):
        return self.text

from django.urls import path
from main.views import *
from django.conf import settings
from django.conf.urls.static import static
from django.urls import path

urlpatterns = [
    path('', HomeView.as_view(), name='home'),
    path('category_detail/<int:id>/', category_view, name='category_detail'),
    path('packages/', PackagesView.as_view(), name='packages'),
]
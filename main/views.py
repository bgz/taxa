from django.shortcuts import render, get_object_or_404
from django.views.generic import ListView

from tour.models import Members, Tour, Comment, Category


class HomeView(ListView):
    template_name = 'main/index.html'

    def get(self, request, *args, **kwargs):
        popular_tours = Tour.objects.filter(popular=True)
        members = Members.objects.all()
        last_tours = Tour.objects.order_by('-created')[:3]
        famous = Tour.objects.filter(famous=True)
        comments = Comment.objects.filter(active=True)
        all_category = Category.objects.all()
        comments_texts = []
        for c in comments:
            comments_texts.append(c.text)

        return render(request, self.template_name, context={
            'members': members,
            'popular_tours': popular_tours,
            'comments_texts': comments_texts,
            'last_tours': last_tours,
            'famous': famous,
            'all_category': all_category,
        })

    def post(self, request, *args, **kwargs):
        return render(request, self.template_name, context={
        })

def category_view(request, id):
    category = get_object_or_404(Category, id=id)

    return render(request, 'tour/category_detail.html', context={
        'category': category,
    })

class PackagesView(ListView):
    template_name = 'main/packages.html'

    def get(self, request, *args, **kwargs):
        popular_tours = Tour.objects.filter(popular=True)
        members = Members.objects.all()
        last_tours = Tour.objects.order_by('-created')[:3]
        famous = Tour.objects.filter(famous=True)
        comments = Comment.objects.filter(active=True)
        all_category = Category.objects.all()
        comments_texts = []
        for c in comments:
            comments_texts.append(c.text)

        return render(request, self.template_name, context={
            'members': members,
            'popular_tours': popular_tours,
            'comments_texts': comments_texts,
            'last_tours': last_tours,
            'famous': famous,
            'all_category': all_category,
        })

    def post(self, request, *args, **kwargs):
        return render(request, self.template_name, context={
        })
